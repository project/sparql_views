<?php

class sparql_views_handler_filter extends views_handler_filter {
  /**
   * This class is the base class for all SPARQL Views filters. Once other
   * filters are created, there may be parts that can be generalized into this
   * class. If not, this class will be deleted and all the child classes will
   * extend directly from views_handler_filter.
   */
}
